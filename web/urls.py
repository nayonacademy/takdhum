from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('course/<category>', views.courseCategory, name='single_category'),
    path('<category>/course/<course>', views.course, name='single_course'),
    path('all-courses', views.all_course, name='all_course'),
    path('popular-courses', views.popular_course, name='popular_course'),
    path('resent-courses', views.recent_course, name='recent_course'),
    path('contact', views.contact, name='contact_page'),
    path('about-us', views.about_us, name='about_page'),
    path('faq', views.faq, name='faq_page'),
    path('login', views.get_login, name='login'),
    path('logout', views.get_logout, name='logout'),
    path('profile', views.get_user_profile, name='profile'),
    path('sign-up', views.get_sign_up, name='sign-up'),
]
