from django.shortcuts import render, get_object_or_404, redirect
from .models import *
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from .forms import UserRegistration
from django.contrib import messages


# Create your views here.
def index(request):
    """Takdhum home page"""
    # Need for all view
    categories = CourseCategory.objects.all()
    basic_info = Basic_info.objects.first()

    sliders = Slider.objects.all()
    courses = Course.objects.all()[::-1]
    events = Event.objects.all().order_by('upload_time')
    projects = Project.objects.all().order_by('-upload_time')
    testimonials = Testimonial.objects.last()
    context = {
        'categories': categories,
        'slides': sliders,
        'courses': courses,
        'info': basic_info,
        'events': events,
        'projects': projects,
        't': testimonials,
    }
    return render(request, 'takdhum/index.html', context)


def courseCategory(request, category=None):
    """Takdhum All course category, resent course, popular course etc."""
    # Need for all view
    categories = CourseCategory.objects.all()
    basic_info = Basic_info.objects.first()

    level = CourseLevel.objects.all()

    requested_category = get_object_or_404(CourseCategory, category_url=category)
    requested_category_course = Course.objects.filter(course_category=requested_category.id)


    # courses = get_object_or_404(CourseCategory, category_url=category)

    context = {
        'categories': categories,
        'level': level,
        'info': basic_info,
        'tag': category,
        'courses': requested_category_course,
        'title': requested_category.category_name,
    }
    if category == 'drawing':
        return render(request, 'takdhum/course_category.html', context)
    else:
        return render(request, 'takdhum/all_course.html', context)


def course(request, category, course):
    """All takdhum single course display here"""
    # Need for all view
    basic_info = Basic_info.objects.first()
    categories = CourseCategory.objects.all()

    Single_course = get_object_or_404(Course, course_url=course)
    course_item = SingleVideo.objects.filter(course_name=Single_course.id)
    context = {
        'categories': categories,
        'courses': Single_course,
        'course_item': course_item,
        'request_category': category,
        'info': basic_info,
        'title': course,
    }
    return render(request, 'takdhum/single_course.html', context)


def all_course(request):
    """Show takddhum all courses"""
    # Need for all view
    basic_info = Basic_info.objects.first()
    categories = CourseCategory.objects.all()

    level = CourseLevel.objects.all()
    courses = Course.objects.all()

    context = {
        'categories': categories,
        'level': level,
        'courses': courses,
        'info': basic_info,
        'tag': 'All',
        'title': 'All courses',
    }
    return render(request, 'takdhum/all_course.html', context)


def popular_course(request):
    """Show takdhum popular courses"""
    # Need for all view
    basic_info = Basic_info.objects.first()
    categories = CourseCategory.objects.all()

    level = CourseLevel.objects.all()
    courses = Course.objects.all()

    context = {
        'categories': categories,
        'level': level,
        'courses': courses,
        'info': basic_info,
        'tag': 'Popular',
        'title': 'Takdhum Popular Courses'
    }
    return render(request, 'takdhum/all_course.html', context)


def recent_course(request):
    """Show takdhum recent courses"""
    # Need for all view
    basic_info = Basic_info.objects.first()
    categories = CourseCategory.objects.all()

    level = CourseLevel.objects.all()
    courses = Course.objects.all()[::-1]

    context = {
        'categories': categories,
        'level': level,
        'courses': courses,
        'info': basic_info,
        'tag': 'Recent',
        'title': 'Takdhum Recent Courses'
    }
    return render(request, 'takdhum/all_course.html', context)


def about_us(request):
    """Takdhum about us page"""
    # Need for all view
    basic_info = Basic_info.objects.first()
    categories = CourseCategory.objects.all()

    about = AboutUs.objects.first()
    context = {
        'info': basic_info,
        'categories': categories,
        'about': about,

    }
    return render(request, 'takdhum/about_us.html', context)


def contact(request):
    """Takdhum contact page"""
    # Need for all view
    basic_info = Basic_info.objects.first()
    categories = CourseCategory.objects.all()

    context = {
        'info': basic_info,
        'categories': categories,
    }
    return render(request, 'takdhum/contact.html', context)


def faq(request):
    """Takdhum FAQ page"""
    # Need for all view
    basic_info = Basic_info.objects.first()
    categories = CourseCategory.objects.all()

    question_ans = FAQ.objects.all()
    context = {
        'info': basic_info,
        'categories': categories,
        'faq': question_ans,
    }
    return render(request, 'takdhum/faq.html', context)


def get_user_profile(request, name=None):
    if request.user.is_authenticated:
        basic_info = Basic_info.objects.first()
        categories = CourseCategory.objects.all()
        # login_user = get_object_or_404(User, username=name)
        # print(login_user)
        context = {
            'info': basic_info,
            'categories': categories,
            'title': 'Profile',
            # 'login_user': login_user,
        }
        return render(request, 'takdhum/user_profile.html', context)
    else:
        return redirect('login')


def get_login(request):
    if request.user.is_authenticated:
        return redirect('index')
    else:
        basic_info = Basic_info.objects.first()
        categories = CourseCategory.objects.all()
        form = UserRegistration(request.POST or None)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            return redirect('login')

        context = {
            'categories': categories,
            'info': basic_info,
            'form': form,
            'title': 'Login'
        }
        if request.method == 'POST':
            user = request.POST.get('username')
            password = request.POST.get('password')
            auth = authenticate(request, username=user, password=password)
            if auth is not None:
                login(request, auth)
                # messages.add_message(request, messages.ERROR, 'Login Successful')
                return redirect('profile')
            else:
                messages.add_message(request, messages.ERROR, 'Username or Password Wrong!')
    return render(request, 'takdhum/login-reg.html', context)


def get_logout(request):
    logout(request)
    return redirect('index')


def get_sign_up(request):
    if request.user.is_authenticated:
        return redirect('login')
    else:
        form = UserRegistration(request.POST or None)
        if form.is_valid():
            instance=form.save(commit=False)
            instance.save()
            messages.add_message(request, messages.INFO, 'Registration Successfully Completed!')
            return redirect('login')
        basic_info = Basic_info.objects.first()
        categories = CourseCategory.objects.all()
        context = {
            'info': basic_info,
            'categories': categories,
            'form': form,
            'title': 'Sign up'
        }
        return render(request, 'takdhum/register.html', context)
