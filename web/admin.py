from django.contrib import admin
from .models import *


# Register your models here.
admin.site.register(SingleVideo)


admin.site.register(Course)

admin.site.register(CourseLevel)


admin.site.register(CourseCategory)

admin.site.register(FAQ)

admin.site.register(AboutUs)

admin.site.register(Basic_info)


admin.site.register(Slider)

admin.site.register(Event)

admin.site.register(Project)

admin.site.register(Testimonial)
